package web;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import metier.Icataloguemetier;
import metier.catalogueImpl;
import metier.produit;

public class controleurservlet extends HttpServlet{
	private Icataloguemetier metier;
	
	@Override
	public void init() throws ServletException {
		metier=new catalogueImpl();
	}
			
			@Override
			protected void doGet(HttpServletRequest request, HttpServletResponse response) 
					throws ServletException, IOException {
				doPost(request, response);
			}
			@Override
			protected void doPost(HttpServletRequest request, HttpServletResponse response) 
					throws ServletException, IOException {
				modelcatalog model=new modelcatalog();
				String action =request.getParameter("action");
				if(action!=null) {
					if(action.equals("chercher")) {
						
						model.setMotcle(request.getParameter("motcle"));
						List<produit> produits=metier.listprodm(model.getMotcle());
						model.setProduits(produits);
						
					}
					else if(action.equals("delete")) {
						String ref=request.getParameter("ref");
						metier.deleteproduit(ref);
						model.setProduits(metier.listproduit());
					}
					else if(action.equals("ajouter")) {
						model.getProduit().setReference(request.getParameter("refrence"));
						model.getProduit().setDesignation(request.getParameter("designation"));
						model.getProduit().setPrix(Double.parseDouble(request.getParameter("prix")));
						model.getProduit().setQuantite(Integer.parseInt(request.getParameter("quantite")));
						model.setMode(request.getParameter("mode"));
						if(model.getMode().equals("ajout"))
						    metier.addproduit(model.getProduit());
						else if(model.getMode().equals("edit"))
							metier.updateproduit(model.getProduit());
						model.setProduits(metier.listproduit());
					}
					else if(action.equals("edit")) {
						String ref=request.getParameter("ref");
						produit p=metier.getproduit(ref);
						model.setProduit(p);
						model.setMode("edit");
						
						model.setProduits(metier.listproduit());
					}
					
				}
				request.setAttribute("model", model);
				request.getRequestDispatcher("vueproduit.jsp").forward(request, response);
			}

}
