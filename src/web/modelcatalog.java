package web;

import java.util.ArrayList;
import java.util.List;

import metier.produit;

public class modelcatalog {
	private String motcle;
	private String mode="ajout";
	private produit produit=new produit();
	private List<produit> produits=new ArrayList<>();
	public modelcatalog() {
		super();
		// TODO Auto-generated constructor stub
	}
	public modelcatalog(String motcle, List<produit> produits) {
		super();
		this.motcle = motcle;
		this.produits = produits;
	}
	public String getMotcle() {
		return motcle;
	}
	public void setMotcle(String motcle) {
		this.motcle = motcle;
	}
	public List<produit> getProduits() {
		return produits;
	}
	public void setProduits(List<produit> produits) {
		this.produits = produits;
	}
	public produit getProduit() {
		return produit;
	}
	public void setProduit(produit produit) {
		this.produit = produit;
	}
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	
	
	
	

}
