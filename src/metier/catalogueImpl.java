package metier;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class catalogueImpl implements Icataloguemetier {

	@Override
	public void addproduit(produit p) {
		Connection conn=singletonconnection.getConnection();
		try {
			PreparedStatement ps=conn.prepareStatement
					("insert into produit (ref_prod,designation,prix,quantite) values(?,?,?,?)");
			ps.setString(1, p.getReference());
			ps.setString(2, p.getDesignation());
			ps.setDouble(3, p.getPrix());
			ps.setInt(4, p.getQuantite());
			ps.executeUpdate();
			ps.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public List<produit> listproduit() {
		List<produit> prods=new ArrayList<produit>();
		Connection conn=singletonconnection.getConnection();
		try {
			PreparedStatement ps=conn.prepareStatement
					("select * from produit");
			ResultSet rs=ps.executeQuery();
			while(rs.next()) {
				produit p=new produit();
				p.setReference(rs.getString("ref_prod"));
				p.setDesignation(rs.getString("designation"));
				p.setPrix(rs.getDouble("prix"));
				p.setQuantite(rs.getInt("quantite"));
				prods.add(p);
			}
			ps.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return prods;
	}

	@Override
	public List<produit> listprodm(String mc) {
		List<produit> prodsmc=new ArrayList<>();
		Connection conn=singletonconnection.getConnection();
		try {
			PreparedStatement ps=conn.prepareStatement
					("select * from produit where designation like ?");
			ps.setString(1, "%"+mc+"%");
			ResultSet rs=ps.executeQuery();
			while(rs.next()) {
				produit p=new produit();
				p.setReference(rs.getString("ref_prod"));
				p.setDesignation(rs.getString("designation"));
				p.setPrix(rs.getDouble("prix"));
				p.setQuantite(rs.getInt("quantite"));
				prodsmc.add(p);
			}
			ps.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return prodsmc;
	}

	@Override
	public produit getproduit(String ref) {
		produit p=null;
		Connection conn=singletonconnection.getConnection();
		try {
			PreparedStatement ps=conn.prepareStatement
					("select * from produit where ref_prod=?");
			ps.setString(1, ref);
			ResultSet rs=ps.executeQuery();
			if(rs.next()) {
				p=new produit();
				p.setReference(rs.getString("ref_prod"));
				p.setDesignation(rs.getString("designation"));
				p.setPrix(rs.getDouble("prix"));
				p.setQuantite(rs.getInt("quantite"));
				
			}
			ps.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(p==null) throw new RuntimeException("produit introuvable");
		return p;
	}

	@Override
	public void updateproduit(produit p) {
	Connection conn=singletonconnection.getConnection();
	try {
		PreparedStatement ps=conn.prepareStatement
				("update produit set designation=?,prix=?,quantite=? where ref_prod=?");
		ps.setString(1, p.getDesignation());
		ps.setDouble(2, p.getPrix());
		ps.setInt(3, p.getQuantite());
		ps.setString(4, p.getReference());
		ps.executeUpdate();
		ps.close();
		
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		
	}

	@Override
	public void deleteproduit(String ref) {
	 Connection conn=singletonconnection.getConnection();
	 try {
		PreparedStatement ps=conn.prepareStatement
				 ("delete from produit where ref_prod=?");
		ps.setString(1, ref);
		ps.executeUpdate();
		ps.close();
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		
	}

}
